# Лабораторная работа 3 часть 3
<section id="comp-knk4kp9y" class="_3d64y">
 <div id="bgLayers_comp-knk4kp9y" data-hook="bgLayers" class="_3wnIc">
  <div data-testid="colorUnderlay" class="_3KzuS _3SQN-">
   </div>
    <div id="bgMedia_comp-knk4kp9y" class="_2GUhU"> 
     </div>
      </div>
        <div data-testid="columns" class="_1uldx">
          <div id="comp-knk4kp9z" class="_1vNJf">
       <div id="bgLayers_comp-knk4kp9z" data-hook="bgLayers" class="_3wnIc">
    <div data-testid="colorUnderlay" class="_3KzuS _3SQN-">
  </div>
<div id="bgMedia_comp-knk4kp9z" class="_2GUhU">
  </div>
    </div>
      <div data-mesh-id="comp-knk4kp9zinlineContent" data-testid="inline-content" class="">
        <div data-mesh-id="comp-knk4kp9zinlineContent-gridContainer" data-testid="mesh-container-content">
          <section id="comp-knk4kpa0" class="_3d64y">
            <div id="bgLayers_comp-knk4kpa0" data-hook="bgLayers" class="_3wnIc">
              <div data-testid="colorUnderlay" class="_3KzuS _3SQN-">
                </div>
                  <div id="bgMedia_comp-knk4kpa0" class="_2GUhU">
                    </div>
                      </div>
                       <div data-testid="columns" class="_1uldx">
                     <div id="comp-knk4kpa1" class="_1vNJf">
                   <div id="bgLayers_comp-knk4kpa1" data-hook="bgLayers" class="_3wnIc">
                 <div data-testid="colorUnderlay" class="_3KzuS _3SQN-">
               </div>
             <div id="bgMedia_comp-knk4kpa1" class="_2GUhU">
           </div>
         </div>
       <div data-mesh-id="comp-knk4kpa1inlineContent" data-testid="inline-content" class="">
     <div data-mesh-id="comp-knk4kpa1inlineContent-gridContainer" data-testid="mesh-container-content">
   <div id="comp-knk4kpa3" class="_1Q9if" data-testid="richTextElement" style="visibility: inherit;" 
data-angle="0" data-angle-style-location="style" data-screen-in-hide="done">
   <h2 class="font_6" style="text-align:center;
line-height:1.35em;
font-size:40px">
       <span class="color_15">Связаться</span></h2>
     </div>
   <div id="comp-knk4kpa4" class="_1Q9if" data-testid="richTextElement" style="visibility: inherit;" data-angle="0" data-angle-style-location="style" data-screen-in-hide="done">
<p class="font_9" style="text-align:center;
line-height:1.875em;font-size:15px"><span class="color_15">ул. Арбат, 1а, Москва, 119019, Россия</span></p></div>
  <div id="comp-knk4kpa5" class="_1Q9if" data-testid="richTextElement" style="visibility: inherit;" data-angle="0" data-angle-style-location="style" data-screen-in-hide="done">
   <p class="font_9" style="text-align:center;
line-height:1.875em;
font-size:15px">
    <span class="color_15">
      <a data-auto-recognition="true" href="mailto:theromandit34@gmail.com">theromandit34@gmail.com</a></span></p></div>
    <div id="comp-knk4kpa6" class="_1Q9if" data-testid="richTextElement" style="visibility: inherit;" data-angle="0" data-angle-style-location="style" data-screen-in-hide="done">
  <p class="font_9" style="text-align:center;
line-height:1.875em;
font-size:15px"><span class="color_15">+7 123 456-78-90</span>
</p>
  </div>
     </div>
        </div>
            </div>
              <div id="comp-knk4kpa7" class="_1vNJf">
                <div id="bgLayers_comp-knk4kpa7" data-hook="bgLayers" class="_3wnIc"><div data-testid="colorUnderlay" class="_3KzuS _3SQN-"></div>
                  <div id="bgMedia_comp-knk4kpa7" class="_2GUhU">
                </div>
              </div>
            <div data-mesh-id="comp-knk4kpa7inlineContent" data-testid="inline-content" class="">
          <div data-mesh-id="comp-knk4kpa7inlineContent-gridContainer" data-testid="mesh-container-content">
        <div id="comp-knk4kv49" class="_11gHK">
      <div data-mesh-id="comp-knk4kv49inlineContent" data-testid="inline-content" class="">
    <div data-mesh-id="comp-knk4kv49inlineContent-gridContainer" data-testid="mesh-container-content">
  <form id="comp-knk4kv59" class="yBJuM">
<div data-mesh-id="comp-knk4kv59inlineContent" data-testid="inline-content" class="">
 <div data-mesh-id="comp-knk4kv59inlineContent-gridContainer" data-testid="mesh-container-content">
  <div id="comp-knk4kv6j" class="_2dBhC _65cjg"><div class="XRJUI"><input type="text" name="имя" id="input_comp-knk4kv6j" class="_1SOvY has-custom-focus" value="" placeholder="Имя" required="" maxlength="100" aria-label="Имя"></div></div>
   <div id="comp-knk4kv6y" class="_2dBhC"><div class="XRJUI"><input type="text" name="адрес" id="input_comp-knk4kv6y" class="_1SOvY has-custom-focus" value="" placeholder="Адрес" maxlength="250" aria-label="Адрес"></div></div>
    <div id="comp-knk4kv7n" class="_2dBhC _65cjg"><div class="XRJUI"><input type="email" name="email" id="input_comp-knk4kv7n" class="_1SOvY has-custom-focus" value="" placeholder="Эл. почта" required="" pattern="^.+@.+\.[a-zA-Z]{2,63}$" maxlength="250" aria-label="Эл. почта"></div></div><div id="comp-knk4kv88" class="_2dBhC"><div class="XRJUI">
     <input type="tel" name="phone" id="input_comp-knk4kv88" class="_1SOvY has-custom-focus" value="" placeholder="Телефон" maxlength="50" aria-label="Телефон"></div></div>
      <div id="comp-knk4kv8j" class="_2dBhC"><div class="XRJUI"><input type="text" name="тема" id="input_comp-knk4kv8j" class="_1SOvY has-custom-focus" value="" placeholder="Тема" aria-label="Тема"></div></div>
       <div id="comp-knk4kv8u" class="bItEI"><label for="textarea_comp-knk4kv8u" class="_20uhs"></label><textarea id="textarea_comp-knk4kv8u" class="_1VWbH has-custom-focus" placeholder="Добавьте сообщение..."></textarea></div>
        <div id="comp-knk4kv9l" class="_1Q9if" data-testid="richTextElement">
         <p class="font_8" style="text-align:center;font-size:18px"><span class="color_15">Отправлено. Спасибо!</span></p></div>
           <div id="comp-knk4kv9u" aria-disabled="false" class="_2UgQw">
            <button aria-disabled="false" data-testid="buttonElement" class="_1fbEI">
             <span class="_1Qjd7">Отправить</span>
              </button>
               </div>
                </div>
                 </div>
                  </form>
                   </div>
                    </div>
                     </div>
                      </div>
                       </div>
                        </div>
                         </div>
                          </section>
                           <section id="comp-knk4kpaa" class="_3d64y">
                            <div id="bgLayers_comp-knk4kpaa" data-hook="bgLayers" class="_3wnIc">
                              <div data-testid="colorUnderlay" class="_3KzuS _3SQN-">
                                </div>
                                 <div id="bgMedia_comp-knk4kpaa" class="_2GUhU">
                                   </div>
                                 </div>
                               <div data-testid="columns" class="_1uldx">
                             <div id="comp-knk4kpab" class="_1vNJf">
                           <div id="bgLayers_comp-knk4kpab" data-hook="bgLayers" class="_3wnIc">
                         <div data-testid="colorUnderlay" class="_3KzuS _3SQN-">
                       </div>
                     <div id="bgMedia_comp-knk4kpab" class="_2GUhU">
                   </div>
                    </div>
                     <div data-mesh-id="comp-knk4kpabinlineContent" data-testid="inline-content" class="">
                      <div data-mesh-id="comp-knk4kpabinlineContent-gridContainer" data-testid="mesh-container-content">
                       <div id="comp-knk4kpac" tabindex="0" class="_1Cu5u">
                        <wix-iframe title="Google Карты" aria-label="Google Карты" data-src="">
                         <div id="mapContainer_comp-knk4kpac" class="_36gi0">
                          <iframe title="Google Карты" aria-label="Google Карты" data-src="" width="100%" height="100%" frameborder="0" scrolling="no" allowfullscreen="" src="https://static.parastorage.com/services/editor-elements/dist/b4f9c49a00780b3a8097e7114b529be7.html?defaultLocation=0&amp;showZoom=true&amp;showStreetView=true&amp;showMapType=true&amp;language=ru&amp;id=dataItem-knk4kpgc&amp;googleMapsScriptUrl=https%3A%2F%2Fstatic.parastorage.com%2Fservices%2Feditor-elements%2Fdist%2Fgoogle-map.min.js">
                          </iframe>
                        </div>
                      </wix-iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div> 
          </section>
        </div>
      </div>
    </div>
  </div>
</section>

